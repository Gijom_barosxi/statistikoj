#! /usr/bin/python

import sqlite3

import urllib2
import re

debug = False

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, ' ', raw_html)
  return cleantext

regex_s = ur'(\d+)\s+Students'
regex_t = ur'(\d+)\s+Trees'
regex_l = ur'(\d+)\s+@ L10+'
regex_go = ur'(\d+)\s+Golden Owls'
regex_gt = ur'(\d+)\s+Golden Trees'

c = None
#SQL
konekto = sqlite3.connect('data.db')
c = konekto.cursor()
c.execute('''CREATE TABLE IF NOT EXISTS duome
             (id integer primary key autoincrement, \
             lingvo text, \
             dato timestamp DEFAULT CURRENT_TIMESTAMP, \
             lernantoj integer, \
             arboj integer, \
             niveloP10 integer, \
             orajStrigoj integer, \
             orajArboj integer)''')


def TestoDeDatumojEnDuome(lingvo, nbr_students, nbr_Trees, nbr_level_sup10, nbr_golden_owl, nbr_golden_trees):
  #Cxu la datumo jam ekzistas en la datumbazo?
  lasta_datumo =  c.execute('''SELECT  lernantoj, arboj, niveloP10, orajStrigoj, orajArboj FROM duome WHERE lingvo='{}' ORDER BY dato DESC LIMIT 1'''.format(lingvo)).fetchall()[0]

  kontrolilo = False # kontrolilo estas 'True' Se la lasta datumo estas simila al nova

  if len(lasta_datumo)==0 : # nova lingvo
    return kontrolilo

  if debug == True :
      print u'Komparo por %s:'%lingvo
      print u'Lasta\tNova'
      print u'{}\t{}\tlernantoj'.format(lasta_datumo[0],nbr_students)
      print u'{}\t{}\tArboj'.format(lasta_datumo[1],nbr_Trees)
      print u'{}\t{}\t@ Nivelo 10+'.format(lasta_datumo[2],nbr_level_sup10)
      print u'{}\t{}\tOraj Strigoj'.format(lasta_datumo[3],nbr_golden_owl)
      print u'{}\t{}\tOraj Arboj'.format(lasta_datumo[4],nbr_golden_trees)
      print '-'*20

  # (lernantoj, arboj, niveloP10, orajStrigoj, orajArboj)
  # (    0    ,   1  ,     2   ,      3      ,      4   )
  if (lasta_datumo[0] == int(nbr_students)) and \
     (lasta_datumo[1] == int(nbr_Trees)) and \
     (lasta_datumo[2] == int(nbr_level_sup10)) and \
     (lasta_datumo[3] == int(nbr_golden_owl)) and \
     (lasta_datumo[4] == int(nbr_golden_trees)) :
      kontrolilo = True

  return kontrolilo

def elsxuti(lnvo):
    url = 'https://duome.eu/' + lnvo
    data = urllib2.urlopen(url)

    text = data.read()
    text = re.findall(ur'Statistics([\w\W]*)<\/ul><\/div>', text, re.MULTILINE)[0]
    text = re.findall(ur'<ul>([\w\W]*)<\/ul>', text, re.MULTILINE)[0]

    nbr_students = re.findall(regex_s, text, re.MULTILINE)[0]
    nbr_Trees = re.findall(regex_t, text, re.MULTILINE)[0]
    nbr_level_sup10 = re.findall(regex_l, text, re.MULTILINE)[0]
    nbr_golden_owl = re.findall(regex_go, text, re.MULTILINE)[0]
    nbr_golden_trees = re.findall(regex_gt, text, re.MULTILINE)[0]

    if debug == True :
      print '*'*20
      print u'Novaj statistikoj por %s:'%lnvo
      print u'{} lernantoj'.format(nbr_students)
      print u'{} Arboj'.format(nbr_Trees)
      print u'{} @ Nivelo 10+'.format(nbr_level_sup10)
      print u'{} Oraj Strigoj'.format(nbr_golden_owl)
      print u'{} Oraj Arboj'.format(nbr_golden_trees)


    if TestoDeDatumojEnDuome(lnvo, nbr_students, nbr_Trees, nbr_level_sup10, nbr_golden_owl, nbr_golden_trees) == False :
      c.execute('''INSERT INTO duome (dato, lingvo, lernantoj, arboj, niveloP10, orajStrigoj, orajArboj) \
                 SELECT CURRENT_TIMESTAMP, ?, ?, ?, ?, ?, ?''', (lnvo, nbr_students, nbr_Trees, nbr_level_sup10, nbr_golden_owl, nbr_golden_trees))
      print u'Novaj datumoj de {} aldonas en la datumbazo.'.format(lnvo)
    else :
      print u'Novaj datumoj de {} jam estas en la datumbazo.'.format(lnvo)
      # Fari poste : aldoni en dosiero kiu oni devos relsxuti cxi tiu lingvo
      ## Refari la elsxuto en cxiu unu horo (por lingvoj kiuj estas en la reelsxuta dosiero) gxis la elsxuto estas nova
      ### Se la datumo estas nova, do forigi la lingvo de la dosiero kaj cron
      ### (Por schtasks, krei taskon kiu mem detruas post esti farita)


#################################################

# Retpagxon
cxefapagxo = urllib2.urlopen('https://duome.eu/')

lingvoj = None
for l in cxefapagxo.read().split("\n"):
    if "Golden Owl" not in l:
        continue
    nehtml = cleanhtml(l)
    lingvoj = re.findall("([a-z]+)\s*[0-9]+",nehtml)
if debug == True :
  print(lingvoj)
  print u'~'*20

for p in lingvoj:
    elsxuti(p)


konekto.commit()
konekto.close()
