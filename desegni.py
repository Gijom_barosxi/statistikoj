#! /usr/bin/python

import sqlite3
#import time
#import datetime
#from scipy import ndimage
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import argparse
import sys
import os

import ConfigParser     # Por la bilda listo


def kreiArgumentojn():
    parser = argparse.ArgumentParser(description='Markdown listo')
    parser.add_argument('--dosiero', nargs='?', default='data.db',
                        help='dosiernomo - (*.sql aux *.db)')
    parser.add_argument('--bildalisto', nargs='?',
                        help='bildalisto dosiero')
    parser.add_argument('--de', nargs=1,
                        help='de dato')
    parser.add_argument('--gxis', nargs=1,
                        help='gxis dato')
    parser.add_argument('--lingvo', nargs='+',
                        help='lingvo de la kurso')
    parser.add_argument('--instruita', nargs='+',
                        help='instruita lingvo')
    parser.add_argument('--grupigi', nargs=1, help='I - instruita lingvo, L - lingvo de la kurso')
    parser.add_argument('--log', action='store_true', help='Por vidi la vertikalan akson en logaritmo')
    parser.add_argument('--tipo', default='sumo', help='Povas esti : sumo, diferenco, procento(sur le tuta mondo de duolingo) aux ordo')
    parser.add_argument('--meza', type=int, nargs='?', default=7, help='Donas la mezumo sur N datumoj')
    parser.add_argument('--bildnomo', \
                        nargs='?', \
                        help='La nomo de la bildo kiu estos generita')

    args = parser.parse_args()

    return args


def kreiLingvanInterpeton(kolumno, lingvoj, sqlargoj):
    #print(lingvoj)
    if lingvoj == None:
        return ""
    sql = " AND ("
    i = 0
    for lingvo in lingvoj:
        if i > 0:
            sql += ' OR '
        sql += kolumno + " = ? "
        sqlargoj.append(lingvo)
        i += 1
    sql += ")"
    return sql


def sumaInterpeto(argoj, procento=False):
    sql = "SELECT datetime(dato), lingvo, parolanto, sum(sumo) "

    if procento:
        sql += "* 100.0 / (SELECT sum(sumo) \
                               FROM \
                                   (SELECT avg(sumo) as sumo,dato \
                                       FROM duolingo as t3 \
                                       WHERE date(t1.dato) = date(t3.dato) \
                                       GROUP BY lingvo, parolanto\
                                    ) as t2 \
                               WHERE date(t1.dato) = date(t2.dato))"

    sql +=" as amount FROM duolingo as t1 WHERE 1=1 "
    sqlargoj = []
    if argoj.de != None:
        sql += " AND dato>=?"
        sqlargoj.append(argoj.de[0])
    if argoj.gxis != None:
        sql += " AND dato<=?"
        sqlargoj.append(argoj.gxis[0])
    sql += kreiLingvanInterpeton('parolanto', argoj.lingvo, sqlargoj)
    sql += kreiLingvanInterpeton('lingvo', argoj.instruita, sqlargoj)
    sql+= """ GROUP BY datetime(dato) """

    if argoj.grupigi == None or argoj.grupigi[0] == 'I':
        sql+= """, lingvo """
    if argoj.grupigi == None or argoj.grupigi[0] == 'L':
        sql+= """, parolanto """
    sql+= """ ORDER BY dato, lingvo, parolanto"""
    return [{'rezulto': True, 'sql':sql, 'argoj': sqlargoj}]

def ordaInterpeto(argoj):
    arr = []
    sql = "BEGIN;"
    arr.append({'rezulto': False,'sql': sql, 'argoj':[]})

    sql = "DROP TABLE IF EXISTS meztablo;"
    arr.append({'rezulto': False,'sql': sql, 'argoj':[]})

    sqlargoj = []
    sql = """
CREATE TEMP TABLE meztablo AS
SELECT dato, lingvo, parolanto, sum(sumo) as sumo FROM (SELECT date(dato) as dato, lingvo, parolanto, avg(sumo) as sumo
FROM duolingo WHERE 1 = 1
"""
    if argoj.de != None:
        sql += " AND dato>=?"
        sqlargoj.append(argoj.de[0])
    if argoj.gxis != None:
        sql += " AND dato<=?"
        sqlargoj.append(argoj.gxis[0])

    sql +=" GROUP BY date(dato), lingvo, parolanto ) "

    sql += " GROUP BY dato"
    if argoj.grupigi == None or argoj.grupigi[0] == 'I':
        sql+= """, lingvo """
    if argoj.grupigi == None or argoj.grupigi[0] == 'L':
        sql+= """, parolanto """
    arr.append({'rezulto': True, 'sql': sql, 'argoj':sqlargoj})

    sqlargoj = []
    sql = """
SELECT datetime(dato), lingvo, parolanto,(
SELECT 0 - count(*) FROM meztablo as t2 WHERE t1.sumo < t2.sumo AND date(t1.dato) = date(t2.dato)) as vico FROM meztablo as t1 WHERE 1 = 1
"""
    sql += kreiLingvanInterpeton('parolanto', argoj.lingvo, sqlargoj)
    sql += kreiLingvanInterpeton('lingvo', argoj.instruita, sqlargoj)

    arr.append({'rezulto': True, 'sql': sql, 'argoj':sqlargoj})
    return arr

def diferencaInterpeto(argoj):
    arr = []
    sql = "BEGIN;"
    arr.append({'rezulto': False,'sql': sql, 'argoj':[]})

    sql = "DROP TABLE IF EXISTS meztablo;"
    arr.append({'rezulto': False,'sql': sql, 'argoj':[]})

    sqlargoj = []
    sql =  """
CREATE TEMP TABLE meztablo AS
SELECT dato, lingvo, parolanto, (sumo-antaua)/tagoj as diferenco FROM
(
  SELECT *,
    (SELECT julianday(t1.dato)-julianday(dato) FROM duolingo as t3 WHERE parolanto=t1.parolanto and lingvo=t1.lingvo and dato < t1.dato ORDER BY dato DESC LIMIT 1) as tagoj,
    (SELECT sumo FROM duolingo as t3 WHERE parolanto=t1.parolanto and lingvo=t1.lingvo and dato < t1.dato ORDER BY dato DESC LIMIT 1) as antaua
FROM duolingo as t1 WHERE 1 = 1
"""
    if argoj.de != None:
        sql += " AND dato>=?"
        sqlargoj.append(argoj.de[0])
    if argoj.gxis != None:
        sql += " AND dato<=?"
        sqlargoj.append(argoj.gxis[0])
    sql += """
) WHERE tagoj not null;
           """
    arr.append({'rezulto': False, 'sql': sql, 'argoj':sqlargoj})

    sqlargoj = []
    sql = """
SELECT dato, lingvo, parolanto, sum(meza) FROM (SELECT datetime(dato) as dato, lingvo, parolanto,
(
 SELECT avg(diferenco) FROM meztablo as t2
 WHERE lingvo = t1.lingvo and parolanto = t1.parolanto and
                      dato <= t1.dato and dato >= date(t1.dato,? ) ORDER BY dato, lingvo, parolanto
) as meza
FROM meztablo as t1) WHERE  1=1
"""
    sqlargoj.append('-' +str(argoj.meza) +' days')
    sql += kreiLingvanInterpeton('parolanto', argoj.lingvo, sqlargoj)
    sql += kreiLingvanInterpeton('lingvo', argoj.instruita, sqlargoj)
    sql += " GROUP BY dato"
    if argoj.grupigi == None or argoj.grupigi[0] == 'I':
        sql+= """, lingvo """
    if argoj.grupigi == None or argoj.grupigi[0] == 'L':
        sql+= """, parolanto """
    arr.append({'rezulto': True, 'sql': sql, 'argoj':sqlargoj})
    return arr

def cxefa(argoj):
    print u'Bilda nomo : {}'.format(argoj.bildnomo)

    sql = None
    sqlargoj = None
    if argoj.tipo == 'diferenco':
        sqlarr = diferencaInterpeto(argoj)
    elif argoj.tipo == 'procento':
        sqlarr = sumaInterpeto(argoj, True)
    elif argoj.tipo == 'ordo':
        sqlarr = ordaInterpeto(argoj)
    else:
        sqlarr = sumaInterpeto(argoj)
    #print(sqlarr)


    INDEKSO_DATO = 0
    INDEKSO_LINGVO = 1
    INDEKSO_PAROLANTO = 2
    INDEKSO_SUMO = 3

    graphs = {}
    fig = plt.figure()
    rect = fig.patch

    i = 0
    t = ''
    for interpeto in sqlarr:
        if interpeto['rezulto'] == True:
            for rikordo in c.execute(interpeto['sql'], interpeto['argoj']):
                print rikordo
                nomo = ""
                if argoj.grupigi == None or argoj.grupigi[0] == 'I':
                    nomo += rikordo[INDEKSO_LINGVO]
                nomo += ":"
                if argoj.grupigi == None or argoj.grupigi[0] == 'L':
                    nomo += rikordo[INDEKSO_PAROLANTO]
                #print(rikordo)
                valoro = rikordo[INDEKSO_DATO] + ',' + str(rikordo[INDEKSO_SUMO])
                if nomo not in graphs:
                    graphs[nomo] = {"arr": []}
                graphs[nomo]["arr"].append(valoro)
        else:
            c.execute(interpeto['sql'], interpeto['argoj'])

    #print(graphs)
    markers = [".",\
               ",",\
               "o",\
               "v",\
               "^",\
               "<",\
               ">",\
               "1",\
               "2",\
               "3",\
               "4",\
               "8",\
               "s",\
               "p",\
               "P",\
               "*",\
               "h",\
               "H",\
               "+",\
               "x",\
               "X",\
               "D",\
               "d",\
               "|",\
               "_",\
               0,\
               1,\
               2,\
               3,\
               4,\
               5,\
               6,\
               7,\
               8,\
               9,\
               10,\
               11]
    for marker, p in zip(markers,graphs):
        #print(graphs[p]["arr"])
        datestamp, value = np.loadtxt(graphs[p]["arr"], \
                                      delimiter=',', \
                                      unpack=True,\
                                      converters={ 0: mdates.strpdate2num('%Y-%m-%d %H:%M:%S')} \
                                      )


        plt.plot_date(x=datestamp, \
                      y=value, \
                      fmt='-', \
                      xdate=True, \
                      ydate=False, \
                      label=p, \
                      linewidth=1, \
                      marker=marker, \
                      markersize=2)


    if argoj.log:
        plt.yscale('log')
    plt.legend(loc='center left', \
               bbox_to_anchor=(1, 0.5), \
               ncol=2)
    plt.grid(True,which="both")
    plt.gcf().autofmt_xdate(rotation=90)
    DefaultSize = plt.gcf().get_size_inches()
    plt.gcf().set_size_inches( (DefaultSize[0]*2, DefaultSize[1]*2) )

    if argoj.bildnomo == None:
        plt.show()
        return


    plt.savefig(argoj.bildnomo, bbox_inches = "tight", dpi=1200)
    plt.clf()
    plt.close()
    print u'Savita bildo'

if __name__ == "__main__":
    argoj = kreiArgumentojn()
    rapida_konekto = None
    filename, ext = os.path.splitext(argoj.dosiero)

    if ext == '.sql':
        datumoj_dosiero = open(argoj.dosiero)
        cmd = datumoj_dosiero.read()
        rapida_konekto = sqlite3.connect(':memory:')
        rapida_konekto.executescript(cmd)
    else:
        rapida_konekto = sqlite3.connect(argoj.dosiero)

    global c
    c = rapida_konekto.cursor()

    if argoj.bildalisto != None:
        bildtipo = u'.png'
        agordoj = ConfigParser.RawConfigParser() # Agorda kreado
        agordoj.read(argoj.bildalisto) # Oni legas la dosieron de agordoj

        listo = {}
        for part in agordoj.sections() :
            listo[part] = agordoj.get(part,'cmd')
            print part
            print listo[part]

        for parto in listo :
            sys.argv = (listo[parto] + ' --bildnomo {}'.format(parto+bildtipo)).split(u' ')
            argoj = kreiArgumentojn()
            cxefa(argoj)


    else :
        argoj = kreiArgumentojn()
        cxefa(argoj)

    rapida_konekto.close()
